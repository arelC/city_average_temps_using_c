/*
 * Arel Jann Clemente | Data Structures & Algorithm Development - C | Prof. Alex Babanski 
 * 
 * This is my way of solving Babanski's Exercise2_19 for Week 2 of this course.
 * 
 */

#include <stdio.h>
#include <stdlib.h>

#define FLUSH while(getchar() != '\n')

int main() {
    // initialize cities and days
    int cities, days;
    
    // get # of cities
    printf("How many cities? ");
    scanf("%d", &cities);
    
    // get # of days
    printf("How many days? ");
    scanf("%d", &days);
    
    // initialize 2D array for city temperatures and days
    int temperatures[cities][days];
    // initialize array for average temperature of a city
    double avg_temperatures[cities];
    // total temperatures of a city
    int total_temperature;
    // initiate and declare counters using registers... cuz it's faster?
    register int city_counter, day_counter, i;
    
    // create a while-loop for temperature inputs
    city_counter = 0;           // set city_counter to 0
    while(city_counter < cities) {
        day_counter = 0;        // set day_counter to 0
        total_temperature = 0;  // set total_temperature to 0
        while(day_counter <  days) {
            // ask for temperature input
            printf("Input temperature for city N%d and day %d: ", 
                    city_counter + 1, day_counter + 1);
            
            // get input
            scanf("%d", &temperatures[city_counter][day_counter]);
            
            // check if input is a valid temperature
            if(temperatures[city_counter][day_counter] < 200 &&
               temperatures[city_counter][day_counter] > -200) {
                total_temperature += temperatures[city_counter][day_counter]; // adds up the total temperatures of a city
                avg_temperatures[city_counter] = total_temperature / days; // get average temperature of a city
                day_counter++; // increment day_counter
            } else {
                printf("Incorrect value!\n");
            }
        }
        city_counter++; // increment city_counter
    } 
    // print output
    printf("-----------------------------\n");
    for(i = 0; i < cities; i++) {
       printf("The average temperature in city N%d was %.2f.\n", i + 1, avg_temperatures[i]); 
    }
    return 0;
}

